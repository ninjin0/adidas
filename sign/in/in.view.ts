namespace $.$mol {
	export class $adidas_sign_in extends $.$adidas_sign_in {
		
		event_submit() {
			this.domain().creds({
				email : this.email() ,
				password : this.password() ,
			})
		}
		
		signed() {
			return this.domain().profile()
		}
		
	}
}
