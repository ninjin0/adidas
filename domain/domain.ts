namespace $ {
	export class $adidas_domain extends $mol_object {
		
		uri( suffix : string ) {
			return `https://adidas-staging.bookingbug.com/api/v1/${ suffix }`
		}
		
		id( ) {
			return 'f6b16c23'
		}
		
		key( ) {
			return 'f0bc4f65f4fbfe7b4b3b7264b655f5eb'
		}
		
		@ $mol_mem_key()
		request( suffix : string ) {
			const next = new $mol_http_request
			
			next.uri = ()=> this.uri( suffix )
			next.method_put = ()=> 'Post'
			
			next.headers = ()=> ({
				'App-Id' : this.id() ,
				'App-Key' : this.key() ,
			})
			
			return next
		}
		
		
		@ $mol_mem()
		creds( next? : { email : string , password : string } ) {
			return $mol_state_session.value( 'creds' , next ) || null
		}
		
		@ $mol_mem()
		sign_in_data() {
			const creds = this.creds()
			if( !creds ) return null
			
			const data = new FormData
			
			data.append( 'email' , creds.email )
			data.append( 'password', creds.password )
			
			return data
		}
		
		@ $mol_mem()
		profile() {
			const data = this.sign_in_data()
			if( !data ) return null
			
			const request = this.request( 'login/' )
			try {
				var text = request.response( data ).responseText
			} catch( error ) {
				try {
					if( JSON.parse( error.message  ).email.length > 0 ) {
						text = error.message
					}
				} catch( error2 ) {
					throw error
				}
			}
			
			return JSON.parse( text )
		}
		
		signed() {
			const profile = this.profile()
			return Boolean( profile && !( profile instanceof Error ) )
		}
		
		@ $mol_mem()
		companies() : { [ id : string ] : any } {
			const next = {} as any
			if( !this.profile()._embedded ) return next
			
			this.profile()._embedded.administrators.forEach( ( company : any )=> {
				next[ company.company_id ] = company
			} )
			
			return next
		}
		
		_events_cache = {} as { [ company_id : string ] : { [ id : string ] : any } }
		
		@ $mol_mem_key()
		events( company_id : string ) : { [ id : string ] : any } {
			let next = this._events_cache[ company_id ]
			if( next ) return next
			
			next = {}
			const end = $jin.time.moment().toString( 'YYYY-MM-DD' )
			const start = $jin.time.moment().shift( - $jin.time.duration( 'P1Y' ) ).toString( 'YYYY-MM-DD' )
			const data = JSON.parse( this.request( `${ company_id }/events/?start_date=${ start }&end_date=${ end }&include_non_bookable=true` ).text() )
			if( !data._embedded ) return next
			
			data._embedded.events.forEach( ( event : any )=> {
				next[ event.id ] = event
			} )
			
			return this._events_cache[ company_id ] = next
		}
		
	}
}
