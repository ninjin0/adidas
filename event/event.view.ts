namespace $.$mol {
	
	export class $adidas_event extends $.$adidas_event {
		
		company_id_current() {
			return $mol_state_arg.value( 'company' )
		}
		
		event_id_current() {
			return $mol_state_arg.value( 'event' )
		}
		
		title() {
			return this.domain().events( this.company_id_current() )[ this.event_id_current() ].description
		}
		
		date() {
			return $jin.time.moment( this.domain().events( this.company_id_current() )[ this.event_id_current() ].datetime ).toString( 'YYYY-MM-DD' )
		}
		
		start() {
			return $jin.time.moment( this.domain().events( this.company_id_current() )[ this.event_id_current() ].datetime ).toString( 'hh:mm' )
		}
		
		event() {
			return this.domain().events( this.company_id_current() )[ this.event_id_current() ]
		}
		
		end() {
			return $jin.time.moment( this.event().datetime ).shift( 'PT' + this.event().duration + 'm' ).toString( 'hh:mm' )
		}
		
		event_join() {
			alert( 'Welcome!' )
		}
		
	}
	
}
