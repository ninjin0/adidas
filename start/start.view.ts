namespace $.$mol {
	
	export class $adidas_start extends $.$adidas_start {
		
		rows() : $mol_view[] {
			return Object.keys( this.domain().companies() ).map( id => this.Row( id ) )
		}
		
		company_title( id : string ) {
			return this.domain().companies()[ id ].company_name
		}
		
		company_id( id : string ) {
			return id
		}
		
	}
	
}
