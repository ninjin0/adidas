namespace $.$mol {
	
	export class $adidas_events extends $.$adidas_events {
		
		sub() {
			if( !this.domain().signed() ) return [ this.Sign_in() ]
			return [ this.App() ]
		}
		
		company_id_current() {
			return $mol_state_arg.value( 'company' )
		}
		
		event_id_current() {
			return $mol_state_arg.value( 'event' )
		}
		
		pages() {
			return [
				this.Placeholder() ,
				this.Start() ,
				this.company_id_current() ? this.Company( this.company_id_current() ) : null ,
				this.event_id_current() ? this.Event() : null ,
			]
		}
		
	}
	
}
