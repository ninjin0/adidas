namespace $.$mol {
	
	export class $adidas_company extends $.$adidas_company {
		
		company_id_current() {
			return $mol_state_arg.value( 'company' )
		}
		
		title() {
			return this.domain().companies()[ this.company_id_current() ].company_name
		}
		
		rows() : $mol_view[] {
			const ids = Object.keys( this.domain().events( this.company_id_current() ) ) 
			const rows : $mol_view[] = ids.map( id => this.Row( id ) )
			if( rows.length === 0 ) rows.push( this.Empty() )
			return rows
		}

		event_descr( id : string ) {
			return this.domain().events( this.company_id_current() )[ id ].description
		}
		
		event_id( id : string ) {
			return id
		}

	}
	
}
